from django.urls import include, path
from django.contrib import admin

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

    # Examples:
    # url(r'^$', 'tk3.views.home', name='home'),
    # url(r'^tk3/', include('tk3.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),

urlpatterns = [
    path('acara/', include('acara.urls')),
    path('peminjaman/', include('peminjaman.urls')),
    path('penugasan/', include('penugasan.urls')),
    # path('sepeda/', include('sepeda.urls')),
    path('stasiun/', include('stasiun.urls')),
    path('voucher/', include('voucher.urls')),
]

