from django.shortcuts import render

# Create your views here.
def daftarStasiun(request):
    return render(request, 'daftar_stasiun.html')
    
def createStasiun(request):
    return render(request, 'create_stasiun.html')

def updateStasiun(request):
    return render(request, 'update_stasiun.html')
