from django.urls import include, path
from .views import createStasiun, daftarStasiun, updateStasiun

urlpatterns = [
    path('', daftarStasiun, name='daftar_stasiun'),
    path('create/', createStasiun, name='create_stasiun'),
    path('update/', updateStasiun, name='update_stasiun'),
]