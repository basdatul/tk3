from django.shortcuts import render

def daftarAcara(request):
    return render(request, 'daftar_acara.html')

def createAcara(request):
    return render(request, 'create_acara.html')

def updateAcara(request):
    return render(request, 'update_acara.html')