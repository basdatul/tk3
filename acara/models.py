from django.db import models

class Acara(models.Model):
    judul = models.CharField(max_length=200, null=True)
    deskripsi = models.CharField(max_length=200, null=True)
    tanggal_mulai = models.DateField(
        max_length=10,
        help_text="format : YYYY-MM-DD",
        null=True)
    tanggal_akhir = models.DateField(
        max_length=10,
        help_text="format : YYYY-MM-DD",
        null=True)
    is_free = [('T', 'Ya'), ('F', 'Tidak')]

    def __str__(self):
        return self.judul