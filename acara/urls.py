from django.urls import include, path
from .views import daftarAcara, createAcara, updateAcara

urlpatterns = [
    path('', daftarAcara, name='daftar_acara'),
    path('create/', createAcara, name='create_acara'),
    path('update/', updateAcara, name='update_acara'),
]
