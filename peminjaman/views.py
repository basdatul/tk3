# Create your views here.

from django.shortcuts import render

def daftarPeminjaman(request):
    return render(request, 'daftar_peminjaman.html')

def createPeminjaman(request):
    return render(request, 'create_peminjaman.html')

def updatePeminjaman(request):
    return render(request, 'daftar_peminjaman.html')