from django.urls import include, path
from .views import daftarPeminjaman, createPeminjaman, updatePeminjaman

urlpatterns = [
    path('', daftarPeminjaman, name='daftar_peminjaman'),
    path('create/', createPeminjaman, name='create_peminjaman'),
    path('update/', updatePeminjaman, name='daftar_peminjaman'),
]