from django.shortcuts import render

# Create your views here.
def daftarPenugasan(request):
    return render(request, 'daftar_penugasan.html')
    
def createPenugasan(request):
    return render(request, 'create_penugasan.html')

def updatePenugasan(request):
    return render(request, 'update_penugasan.html')
