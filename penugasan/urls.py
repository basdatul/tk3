from django.urls import include, path
from .views import createPenugasan, daftarPenugasan, updatePenugasan

urlpatterns = [
    path('', daftarPenugasan, name='daftar_penugasan'),
    path('create/', createPenugasan, name='create_penugasan'),
    path('update/', updatePenugasan, name='update_penugasan'),
]