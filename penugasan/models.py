from django.db import models

class Penugasan(models.Model):
    petugas = models.CharField(max_length=200, null=True)
    tanggal_mulai = models.DateField(
        max_length=10,
        help_text="format : YYYY-MM-DD",
        null=True)
    tanggal_akhir = models.DateField(
        max_length=10,
        help_text="format : YYYY-MM-DD",
        null=True)
    stasiun = models.CharField(max_length=200, null=True)