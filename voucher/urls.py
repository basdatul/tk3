from django.urls import include, path
from .views import createVoucher, daftarVoucher, updateVoucher

urlpatterns = [
    path('', daftarVoucher, name='daftar_voucher'),
    path('create/', createVoucher, name='create_voucher'),
    path('update/', updateVoucher, name='update_voucher'),
]