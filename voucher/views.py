from django.shortcuts import render

# Create your views here.
def daftarVoucher(request):
    return render(request, 'daftar_voucher.html')
    
def createVoucher(request):
    return render(request, 'create_voucher.html')

def updateVoucher(request):
    return render(request, 'update_voucher.html')
