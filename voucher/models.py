from django.db import models

# Create your models here.
class Voucher (models.Model):
    nama = models.CharField(max_length=255)
    kategori = models.CharField(max_length=255)
    nilai_poin = models.IntegerField()
    deskripsi = models.CharField(max_length=255)
    jumlah = models.IntegerField()

    def __str__(self):
        return self.nama